class Users < Api
  format :json
  default_format :json

  helpers do
    def test_params
      @permitted_params ||= declared(params, include_missing: false, include_parent_namespaces: false)
    end
  end

  namespace :users, desc: "Bags related CRUD operations" do
    get "/:id" do
      @user = User.find_by(id: params[:id])
      if @user
        { data: @user, status_code: true }
      else
        { message: " No record found", status_code: false }
      end
    end

    desc "registration end point"
    post "/" do
      @user = User.find_by(email: params[:email], phone: params[:phone])

      if @user.present?
        error!({ message: "User already exists", status_code: false }, 400)
      else
        @user = User.new(params)
        if @user.save!
          { message: @user, status_code: false }
        else
          error!({ message: "Something went wrong", status_code: false }, 400)
        end
      end
    end

    desc "Login endpoint"
    post "/login" do
      @user = User.find_by(email: params[:email])
      if @user && @user.valid_password?(params[:password])
        { message: "User is logged in successfully", user: @user, status_code: true }
      else
        error!({ message: "User email and password don't match", status_code: false }, 400)
      end
    end
  end
end
